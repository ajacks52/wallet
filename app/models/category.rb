class Category < ActiveRecord::Base
	has_many :transactions, dependent: :destroy
	has_many :incomes, dependent: :destroy
	validates :name, presence: true
end
