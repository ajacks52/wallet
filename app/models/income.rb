class Income < ActiveRecord::Base
	validates :name, presence: true
    validates :amount, presence: true
	belongs_to :category
	# def income
	# 	@income
	# end
end
