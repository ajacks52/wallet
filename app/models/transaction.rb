class Transaction < ActiveRecord::Base
  belongs_to :category
  validates :name, presence: true
  validates :amount, presence: true
  validates :category, presence: true
  # def amount
  # 	@amount
  # end
end
