json.array!(@incomes) do |income|
  json.extract! income, :id, :name, :amount
  json.url income_url(income, format: :json)
end
